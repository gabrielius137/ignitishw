﻿using Microsoft.AspNetCore.Razor.TagHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wireframe.Web
{
    [HtmlTargetElement("button")]
    [HtmlTargetElement("input")]
    [HtmlTargetElement("select")]
    [HtmlTargetElement("fieldset")]
    [HtmlTargetElement("option")]
    [HtmlTargetElement("optgroup")]
    [HtmlTargetElement("textarea")]
    public class IsDisabledTagHelper : TagHelper
    {
        [HtmlAttributeName("asp-is-disabled")]
        public bool IsDisabled { set; get; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            if (IsDisabled)
            {
                var attribute = new TagHelperAttribute("disabled");
                output.Attributes.Add(attribute);
            }
            base.Process(context, output);
        }
    }
}
