﻿function notify(type, message) {
    let n = document.createElement("div");
    let id = Math.random().toString(36).substr(2, 10);
    n.setAttribute("id", id);
    n.classList.add("notification", type);
    n.innerText = message;
    let close = document.createElement("div");
    close.classList.add("close");
    n.appendChild(close);
    close.addEventListener("click", () => closeNotification(id));
    document.getElementById("notification-area").appendChild(n);
    setTimeout(() => {
        closeNotification(id);
    }, 5000);
}

function closeNotification(id) {
    let notification = $('#' + id)
    if (notification != null)
        notification.remove();
}

function notifySuccess() {
    notify("success", "Operacija įvykdyta sėkmingai");
}
function notifyError() {
    notify("error", "Operacija nepavyko");
}

$(document).ready(() => {
    $('#document-table').DataTable({
        searching: true,
        ordering: true,
        paging: false,
        info: false,
        language: {
            searchPlaceholder: "Ieškoti...",
            search: "",
            emptyTable: "Nėra duomenų",
            zeroRecords: "Nėra rezultatų",
        },
        columnDefs: [
            {
                orderable: false,
                searchable: false,
                width: '50%',
                targets: [ 1 ]
            }
        ],
        autoWidth: false
    });

    $('.dataTables_filter').addClass('pull-left');

    $('#toggle-button').click(() => {
        $('.toggleable').prop('disabled', (_, val) => !val)
    })
});