﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Wireframe.Persistence;
using Wireframe.Web.Models;

namespace Wireframe.Web.Controllers
{
    [Route("")]
    public class DocumentController : Controller
    {
        private const string regid = "regid";
        private const string Notification = "Notification";
        private readonly IDocumentRepository _documentRepository;
        private readonly IDocumentAttributeValueRepository _documentAttributeValueRepository;

        public DocumentController(IUnitOfWork unit)
        {
            _documentRepository = new DocumentRepository(unit);
            _documentAttributeValueRepository = new DocumentAttributeValueRepository(unit);
        }

        [HttpGet("RegPoz")]
        public async Task<IActionResult> Read([FromQuery(Name = regid)] int documentId)
        {
            var fullDoc = await _documentRepository.ReadDocument(documentId);

            if (fullDoc.Document == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NotFound;
                return View("NotFound");
            }

            return View("Document", fullDoc);
        }

        [HttpPost("RegPoz")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Save([FromQuery(Name = regid)] int documentId, FullDocument doc)
        {
            var entries = doc.AttributeValueOptions.Select(opt => new DocumentAttributeValue() { DocumentId = documentId, AttributeId = opt.Attribute.Id, ValueId = opt.CurrentValue.Id }).ToArray();
            TempData[Notification] = await _documentAttributeValueRepository.UpsertDocumentAttributeValues(entries) ? "notifySuccess()" : "notifyError()";
            return RedirectToAction(nameof(Read), new { regid = documentId });
        }

        [HttpGet]
        public async Task<IActionResult> Documents()
        {
            var docs = await _documentRepository.ReadAll();
            return View(docs);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
