﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public class Scheme
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
