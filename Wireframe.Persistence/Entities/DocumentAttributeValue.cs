﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public class DocumentAttributeValue
    {
        public int DocumentId { get; set; }
        public Document Document { get; set; }
        public int AttributeId { get; set; }
        public int ValueId { get; set; }
        public AttributeValue AttributeValue { get; set; }
    }
}
