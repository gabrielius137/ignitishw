﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public class AttributeValue
    {
        public int AttributeId { get; set; }
        public Attribute Attribute { get; set; }
        public int ValueId { get; set; }
        public Value Value { get; set; }
    }
}
