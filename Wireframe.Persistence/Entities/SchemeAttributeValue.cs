﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public class SchemeAttributeValue
    {
        public int SchemeId { get; set; }
        public Scheme Scheme { get; set; }
        public int AttributeId { get; set; }
        public int ValueId { get; set; }
        public AttributeValue AttributeValue { get; set; }
    }
}
