﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public class Document
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int SchemeId { get; set; }
        public Scheme Scheme { get; set; }
    }
}
