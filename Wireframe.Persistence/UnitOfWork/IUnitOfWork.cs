﻿using System;
using System.Data;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        void Rollback();
        IDbConnection Connection { get; }
        IDbTransaction Transaction { get; }
    }
}