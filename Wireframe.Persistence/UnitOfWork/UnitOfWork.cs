﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Wireframe.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly string _connString;
        private IDbConnection _connection;
        private IDbTransaction _transaction;
        private bool _disposed;

        public UnitOfWork(string connString)
        {
            _connString = connString;
        }

        ~UnitOfWork()
        {
            dispose();
        }

        public IDbConnection Connection
        {
            get
            {
                if (_connection == null)
                    _connection = new SqlConnection(_connString);

                if (_connection.State == ConnectionState.Broken)
                {
                    _connection.Dispose();
                    _connection = new SqlConnection(_connString);
                }

                if (_connection.State == ConnectionState.Closed)
                    _connection.Open();

                if (_transaction == null)
                    return _connection;
                else
                    return _transaction.Connection;
            }
        }

        public IDbTransaction Transaction
        {
            get
            {
                if (_transaction == null)
                    _transaction = Connection.BeginTransaction();

                return _transaction;
            }
        }

        public void Commit()
        {
            if (_transaction == null)
                return;

            try
            {
                _transaction.Commit();
            }
            catch
            {
                _transaction.Rollback();
                throw;
            }
            finally
            {
                _transaction.Dispose();
                _transaction = null;
                _connection.Dispose();
                _connection = null;
            }
        }

        public void Rollback()
        {
            if (_transaction == null)
                return;

            _transaction.Rollback();
            _transaction.Dispose();
            _transaction = null;
            _connection.Dispose();
            _connection = null;
        }

        public void Dispose()
        {
            dispose();
            GC.SuppressFinalize(this);
        }

        private void dispose()
        {
            if (_disposed)
                return;

            if (_transaction != null)
            {
                _transaction.Dispose();
                _transaction = null;
            }
            if (_connection != null)
            {
                _connection.Dispose();
                _connection = null;
            }

            _disposed = true;
        }
    }
}
