﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public class AttributeValueSet
    {
        public AttributeValueSet()
        {
            ValueOptions = new List<Value>();
        }
        public Attribute Attribute { get; set; }
        public IList<Value> ValueOptions { get; set; }
        public Value CurrentValue { get; set; }
    }
}
