﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public class FullDocument
    {
        public FullDocument()
        {
            AttributeValueOptions = new List<AttributeValueSet>();
        }

        public Document Document { get; set; }
        public IList<AttributeValueSet> AttributeValueOptions { get; set; }
    }
}
