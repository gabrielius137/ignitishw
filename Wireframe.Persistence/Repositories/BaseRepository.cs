﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly IUnitOfWork _unit;

        protected BaseRepository(IUnitOfWork unit)
        {
            _unit = unit;
        }

        public abstract Task<int> Create(TEntity entity);
        public abstract Task<int> Delete(TEntity entity);
        public abstract Task<TEntity> Read(params int[] keys);
        public abstract Task<IEnumerable<TEntity>> ReadAll();
        public abstract Task<int> Update(TEntity entity);
    }
}
