﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Wireframe.Persistence
{
    public class DocumentRepository : BaseRepository<Document>, IDocumentRepository
    {
        public DocumentRepository(IUnitOfWork unit) : base(unit)
        {
        }

        public override async Task<int> Create(Document entity)
        {
            if (entity == null || entity.Name == null || entity.SchemeId < 1)
                return 0;

            using (var conn = _unit.Connection)
                return await conn.ExecuteAsync(
                    sql: "insert into Document(Name, SchemeId) values(@name, @schemeId)",
                    param: new { name = entity.Name, schemeId = entity.SchemeId }
                );
        }

        public override async Task<int> Delete(Document entity)
        {
            if (entity == null || entity.Id < 1)
                return 0;

            using (var conn = _unit.Connection)
                return await conn.ExecuteAsync(
                    sql: "delete from Document where Id = @id",
                    param: new { id = entity.Id }
                );
        }

        public override async Task<Document> Read(params int[] keys)
        {
            if (keys.Length != 1)
                throw new ArgumentException($"{nameof(DocumentRepository)} {nameof(Read)} expected single argument got {keys.Length} instead");

            using (var conn = _unit.Connection)
            {
                var document = await conn.QueryAsync<Document, Scheme, Document>(
                    sql: "select t1.Id, t1.Name, t1.SchemeId, t2.Id, t2.Name from Document as t1 join Scheme as t2 on t1.SchemeId = t2.Id and t1.Id = @id",
                    (doc, scheme) => { doc.Scheme = scheme; return doc; },
                    param: new { id = keys[0] }
                );

                return document.FirstOrDefault();
            }
        }

        public override async Task<IEnumerable<Document>> ReadAll()
        {
            using (var conn = _unit.Connection)
                return await conn.QueryAsync<Document, Scheme, Document>(
                    sql: "select t1.Id, t1.Name, t1.SchemeId, t2.Id, t2.Name from Document as t1 join Scheme as t2 on t1.SchemeId = t2.Id",
                    (doc, scheme) => { doc.Scheme = scheme; return doc; }
                );
        }

        public override async Task<int> Update(Document entity)
        {
            if (entity == null || entity.Name == null || entity.SchemeId < 1)
                return 0;

            using (var conn = _unit.Connection)
                return await conn.ExecuteAsync(
                    sql: "update Document set Name = @name, SchemeId = @schemeId where Id = @id",
                    param: new { name = entity.Name, schemeId = entity.SchemeId, id = entity.Id }
                );
        }

        public async Task<FullDocument> ReadDocument(int id)
        {
            if (id < 1)
                return null;

            var fullDoc = new FullDocument();

            using (var conn = _unit.Connection)
            using (var grid = await conn.QueryMultipleAsync(
                sql: "Read_Document",
                param: new { documentId = id },
                commandType: CommandType.StoredProcedure
            ))
            {
                fullDoc.Document = grid.Read<Document, Scheme, Document>(
                    func: (document, scheme) => { document.Scheme = scheme; return document; },
                    splitOn: "Id"
                ).FirstOrDefault();

                var dict = new Dictionary<int, AttributeValueSet>();

                grid.Read<AttributeValue, Attribute, Value, AttributeValue>(
                    func: (attribute_value, attribute, value) => {
                        if (dict.ContainsKey(attribute.Id))
                            dict[attribute.Id].ValueOptions.Add(value);
                        else
                        {
                            var attributeValueSet = new AttributeValueSet() { Attribute = attribute/*, CurrentValue = new Value()*/ };
                            attributeValueSet.ValueOptions.Add(value);
                            dict.Add(attribute.Id, attributeValueSet);
                        }
                        return attribute_value;
                    },
                    splitOn: "Id,Id" 
                );

                grid.Read<AttributeValue, Attribute, Value, AttributeValue>(
                    func: (attribute_value, attribute, value) => {
                        if (dict.ContainsKey(attribute.Id))
                            dict[attribute.Id].CurrentValue = value;
                        return attribute_value;
                    },
                    splitOn: "Id,Id"
                );

                fullDoc.AttributeValueOptions = dict.Values.ToList();
            }

            return fullDoc;
        }
    }

    public interface IDocumentRepository : IRepository<Document>
    {
        Task<FullDocument> ReadDocument(int id);
    }
}
