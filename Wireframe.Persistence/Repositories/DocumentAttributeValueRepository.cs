﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;

namespace Wireframe.Persistence
{
    public class DocumentAttributeValueRepository : BaseRepository<DocumentAttributeValue>, IDocumentAttributeValueRepository
    {
        public DocumentAttributeValueRepository(IUnitOfWork unit) : base(unit) { }

        public override Task<int> Create(DocumentAttributeValue entity)
        {
            throw new NotImplementedException();
        }

        public override Task<int> Delete(DocumentAttributeValue entity)
        {
            throw new NotImplementedException();
        }

        public override Task<DocumentAttributeValue> Read(params int[] keys)
        {
            throw new NotImplementedException();
        }

        public override Task<IEnumerable<DocumentAttributeValue>> ReadAll()
        {
            throw new NotImplementedException();
        }

        public override Task<int> Update(DocumentAttributeValue entity)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> UpsertDocumentAttributeValues(IEnumerable<DocumentAttributeValue> entries)
        {
            if (entries == null)
                return true;

            using (var conn =_unit.Connection)
            using (var tran = _unit.Transaction)
            {
                foreach (var entry in entries)
                {
                    var result = await tran.Connection.ExecuteScalarAsync<int>(
                        sql: "Upsert_DocumentAttributeValue",
                        param: new { documentId = entry.DocumentId, attributeId = entry.AttributeId, valueId = entry.ValueId },
                        transaction: tran,
                        commandType: CommandType.StoredProcedure
                    );

                    if (result != 0)
                    {
                        _unit.Rollback();
                        return false;
                    }
                }

                _unit.Commit();
            }

            return true;
        }
    }

    public interface IDocumentAttributeValueRepository : IRepository<DocumentAttributeValue>
    {
        Task<bool> UpsertDocumentAttributeValues(IEnumerable<DocumentAttributeValue> entries);
    }
}
