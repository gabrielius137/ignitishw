﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wireframe.Persistence
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<int> Create(TEntity entity);
        Task<int> Update(TEntity entity);
        Task<IEnumerable<TEntity>> ReadAll();
        Task<TEntity> Read(params int[] keys);
        Task<int> Delete(TEntity entity);

    }
}
