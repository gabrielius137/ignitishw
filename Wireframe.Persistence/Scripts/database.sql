﻿create table Attribute
(
	Id int identity(1,1) not null,
	Name nvarchar(127) not null,
	constraint PK_Attribute primary key clustered (Id),
	constraint UK_Attribute_Name unique nonclustered (Name desc)
)

insert into Attribute(Name)
VALUES
(N'Reikia atlikti rangos darbus'),--1
(N'Rangos darbus atliks'),--2
(N'Verslo klientas'),--3
(N'Skaičiavimo metodas'),--4
(N'Svarbus klientas'),--5
(N'Užsakyta')--6

create table Value
(
	Id int identity(1,1) not null,
	Name nvarchar(127) not null,
	constraint PK_Value primary key clustered (Id),
	constraint UK_Value_Name unique nonclustered (Name desc)
)

insert into Value(Name)
VALUES
(N'Taip'),--1
(N'Ne'),--2
(N'Metinis rangovas'),--3
(N'Mėnesinis rangovas'),--4
(N'Automatinis'),--5
(N'Rankinis'),--6
(N'Yra'),--7
(N'Nėra')--8

create table AttributeValue
(
	AttributeId int not null,
	ValueId int not null,
	constraint PK_AttributeValue primary key clustered (AttributeId, ValueId),
	constraint FK_AttributeValue_Attribute foreign key (AttributeId)
		references Attribute(Id)
		on delete no action,
	constraint FK_AttributeValue_Value foreign key (ValueId)
		references Value(Id)
		on delete no action
)

insert into AttributeValue(AttributeId, ValueId)
VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 1),
(3, 2),
(4, 5),
(4, 6),
(5, 1),
(5, 2),
(6, 7),
(6, 8)

create table Scheme
(
	Id int identity(1,1) not null,
	Name nvarchar(127) not null,
	constraint PK_Scheme primary key clustered (Id),
	constraint UK_Scheme_Name unique nonclustered (Name desc)
)

insert into Scheme(Name)
VALUES
(N'Pirmoji schema'),--1
(N'Antroji schema'),--2
(N'tuščia schema')--3

create table SchemeAttributeValue
(
	SchemeId int not null,
	AttributeId int not null,
	ValueId int not null,
	constraint PK_SchemeAttributeValue primary key clustered (SchemeId, AttributeId, ValueId),
	constraint FK_SchemeAttributeValue_Scheme foreign key (SchemeId)
		references Scheme(Id)
		on delete no action,
	constraint FK_SchemeAttributeValue_AttributeValue foreign key (AttributeId, ValueId)
		references AttributeValue(AttributeId, ValueId)
		on delete no action
)

insert into SchemeAttributeValue(SchemeId, AttributeId, ValueId)
VALUES
(1, 1, 1),
(1, 1, 2),
(1, 2, 3),
(1, 2, 4),
(1, 3, 1),
(1, 3, 2),
(1, 4, 5),
(1, 4, 6),
(1, 5, 1),
(1, 5, 2),
(2, 3, 1),
(2, 3, 2),
(2, 4, 5),
(2, 4, 6),
(2, 5, 1),
(2, 5, 2),
(2, 6, 7),
(2, 6, 8)

create table Document
(
	Id int identity(1,1) not null,
	Name nvarchar(127) not null,
	SchemeId int not null,
	constraint PK_Document primary key clustered (Id),
	constraint UK_Document_Name unique nonclustered (Name desc),
	constraint FK_Document_Scheme foreign key (SchemeId)
		references Scheme(Id)
		on delete no action
)

insert into Document(Name, SchemeId)
VALUES
(N'Pirmasis dokumentas', 1),--1
(N'Antrasis dokumentas', 2),--2
(N'tuščias dokumentas', 3)--3

create table DocumentAttributeValue
(
	DocumentId int not null,
	AttributeId int not null,
	ValueId int null,
	constraint PK_DocumentAttributeValue primary key clustered (DocumentId, AttributeId),
	constraint FK_DocumentAttributeValue_Document foreign key (DocumentId)
		references Document(Id)
		on delete no action,
	constraint FK_DocumentAttributeValue_AttributeValue foreign key (AttributeId, ValueId)
		references AttributeValue(AttributeId, ValueId)
		on delete no action
)

insert into DocumentAttributeValue(DocumentId, AttributeId, ValueId)
VALUES
(1, 1, 1),
(1, 2, 3),
(1, 3, 2),
(1, 4, 5)
go

CREATE PROCEDURE Read_Document 
	@documentId int
AS
BEGIN
	SET NOCOUNT ON;

	-- select document and schema
    select t1.Id, t1.Name, t1.SchemeId, t2.Id, t2.Name
	from Document as t1
	join Scheme as t2 on t1.Id = @documentId and t1.SchemeId = t2.Id

	-- select attributes and their possible values
	select t3.Id as AttributeId, t3.Id, t3.Name, t4.Id as ValueId, t4.Id, t4.Name
	from SchemeAttributeValue as t1
	join Document as t2 on t2.Id = @documentId and t1.SchemeId = t2.SchemeId
	join Attribute as t3 on t1.AttributeId = t3.Id
	join Value as t4 on t1.ValueId = t4.Id

	-- select attributes and their current values
	select t1.Id as AttributeId, t1.Id, t1.Name, t3.Id as ValueId, t3.Id, t3.Name
	from (
		select t3.Id, t3.Name
		from Document as t1
		join SchemeAttributeValue as t2 on t1.Id = @documentId and t2.SchemeId = t1.SchemeId
		join Attribute as t3 on t2.AttributeId = t3.Id
		group by t3.Id, t3.Name
	) as t1
	left join DocumentAttributeValue as t2 on @documentId = t2.DocumentId and t1.Id = t2.AttributeId
	left join Value as t3 on t2.ValueId = t3.Id
END
go

CREATE PROCEDURE Upsert_DocumentAttributeValue 
	-- Add the parameters for the stored procedure here
	@documentId int,
	@attributeId int,
	@valueId int
AS
BEGIN
	SET NOCOUNT ON;

	-- possible null update
	if @valueId < 1
		set @valueId = null

	-- check if attribute and value pair is related with document's schema
	if exists(
		select 1
		from SchemeAttributeValue as t1
		join Document as t2 on t2.Id = @documentId and t1.SchemeId = t2.SchemeId and t1.AttributeId = @attributeId and (t1.ValueId = @valueId or @valueId is null)
	)
	begin
		update DocumentAttributeValue
		set ValueId = @valueId
		where DocumentId = @documentId and AttributeId = @attributeId

		if @@ROWCOUNT = 0
			insert into DocumentAttributeValue(DocumentId, AttributeId, ValueId)
			values(@documentId, @attributeId, @valueId)

		select 0
	end
	-- illegal update
	else
		select 1

	return
END
go