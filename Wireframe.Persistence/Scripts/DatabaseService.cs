﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;

namespace Wireframe.Persistence
{
    public class DatabaseService
    {
        private static readonly Regex REGEX = new Regex(@"^\s*GO\s*$",
            RegexOptions.Compiled |
            RegexOptions.Multiline |
            RegexOptions.IgnorePatternWhitespace |
            RegexOptions.IgnoreCase);
        private const string KEYWORD = "Initial Catalog";
        private const string SCRIPT = "Wireframe.Persistence.Scripts.database.sql";
        private readonly DbConnectionStringBuilder _stringBuilder;

        public DatabaseService(string connString)
        {
            _stringBuilder = new SqlConnectionStringBuilder(connString);
        }

        /// <summary>
        /// Ensures that database is created
        /// </summary>
        /// <returns>bool explaining if database was created</returns>
        public bool EnsureDatabaseCreated()
        {
            if (DoesDatabaseExist())
                return false;

            var database = _stringBuilder[KEYWORD].ToString();
            _stringBuilder.Remove(KEYWORD);

            using (var unit = new UnitOfWork(_stringBuilder.ConnectionString))
            {
                unit.Connection.Execute(sql: $"create database {database}");
                unit.Connection.ChangeDatabase(database);
                _stringBuilder[KEYWORD] = database;
                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(SCRIPT))
                using (var reader = new StreamReader(stream))
                {
                    foreach(var sql in SplitSqlStatements(reader.ReadToEnd()))
                        unit.Transaction.Connection.Execute(sql: sql, transaction: unit.Transaction);
                }

                unit.Commit();
            }

            return true;
        }

        /// <summary>
        /// Ensures that database is delete
        /// </summary>
        /// <returns>bool explaining if database was deleted</returns>
        public bool EnsureDatabaseDeleted()
        {
            if (!DoesDatabaseExist())
                return false;

            using (var unit = new UnitOfWork(_stringBuilder.ConnectionString))
            {
                var database = _stringBuilder[KEYWORD].ToString();
                unit.Connection.Execute(sql: $"ALTER DATABASE {database} SET SINGLE_USER WITH ROLLBACK IMMEDIATE;");
                unit.Connection.ChangeDatabase("master");
                unit.Connection.Execute(sql: $"DROP DATABASE {database};");
            }

            return true;
        }

        /// <summary>
        /// Check if database exists
        /// </summary>
        /// <returns>bool explaining if database exists</returns>
        public bool DoesDatabaseExist()
        {
            var database = _stringBuilder[KEYWORD].ToString();
            _stringBuilder.Remove(KEYWORD);

            using (var unit = new UnitOfWork(_stringBuilder.ConnectionString))
            {
                _stringBuilder[KEYWORD] = database;
                return unit.Connection.ExecuteScalar<bool>(sql: "select 1 from sys.databases where name = @database", param: new { database });
            }

        }

        private static IEnumerable<string> SplitSqlStatements(string sqlScript)
        {
            // Split by "GO" statements
            var statements = REGEX.Split(sqlScript);

            // Remove empties, trim, and return
            return statements
                .Where(x => !string.IsNullOrWhiteSpace(x))
                .Select(x => x.Trim(' ', '\r', '\n'));
        }
    }
}
