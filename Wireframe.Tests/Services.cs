﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wireframe.Persistence;

namespace Wireframe.Tests
{
    public static class Services
    {
        public const string CONN_STRING = "Server=localhost; Initial Catalog=Test; Integrated Security=True;";
        public static readonly DatabaseService DB_SERVICE;

        static Services()
        {
            DB_SERVICE = new DatabaseService(CONN_STRING);
        }
    }
}
