﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Threading.Tasks;
using Wireframe.Persistence;

namespace Wireframe.Tests
{
    [TestClass]
    public class DatabaseServiceTests
    {
        [TestInitialize()]
        public void Initialize()
        {
            Services.DB_SERVICE.EnsureDatabaseDeleted();
        }

        [TestCleanup()]
        public void Cleanup()
        {
            Services.DB_SERVICE.EnsureDatabaseDeleted();
        }

        [TestMethod]
        public void EnsureDatabaseCreated()
        {
            var result = Services.DB_SERVICE.EnsureDatabaseCreated();
            Assert.IsTrue(result, "Database must be created");

            var repo = new DocumentRepository(new UnitOfWork(Services.CONN_STRING));
            var docs = repo.ReadAll().Result;
            Assert.IsTrue(docs != null && docs.Any(), "Query with test db must return atleast 1 doc");
        }

        [TestMethod]
        public void EnsureDatabaseDeleted()
        {
            Services.DB_SERVICE.EnsureDatabaseCreated();
            var result = Services.DB_SERVICE.EnsureDatabaseDeleted();
            Assert.IsTrue(result, "Database must be deleted");
        }

        [TestMethod]
        public void DoesDatabaseExist()
        {
            Services.DB_SERVICE.EnsureDatabaseCreated();
            var result = Services.DB_SERVICE.DoesDatabaseExist();
            Assert.IsTrue(result, "Database must exist");
            Services.DB_SERVICE.EnsureDatabaseDeleted();
            result = Services.DB_SERVICE.DoesDatabaseExist();
            Assert.IsFalse(result, "Database must not exist");
        }
    }
}
