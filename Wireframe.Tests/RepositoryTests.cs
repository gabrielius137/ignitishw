﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wireframe.Persistence;

namespace Wireframe.Tests
{
    [TestClass]
    public class RepositoryTests
    {
        [TestInitialize()]
        public void Initialize()
        {
            Services.DB_SERVICE.EnsureDatabaseCreated();
        }

        [TestCleanup()]
        public void Cleanup()
        {
            Services.DB_SERVICE.EnsureDatabaseDeleted();
        }

        [TestMethod]
        public void ReadDocuments()
        {
            var repo = new DocumentRepository(new UnitOfWork(Services.CONN_STRING));
            var docs = repo.ReadAll().Result;
            Assert.IsTrue(docs != null && docs.Any(), "Query with test db must return atleast 1 doc");
        }

        [TestMethod]
        public void ReadFullDocument()
        {
            var repo = new DocumentRepository(new UnitOfWork(Services.CONN_STRING));
            var doc = repo.ReadDocument(1).Result;
            Assert.IsTrue(doc.Document != null, "Must refer to a document");
            Assert.IsTrue(doc.Document.Scheme != null, "Must refer to a schema");
            Assert.IsFalse(doc.AttributeValueOptions.Any(x => x.Attribute == null), "Each set must refer to an attribute");
            Assert.IsFalse(doc.AttributeValueOptions.Any(x => x.ValueOptions.Count < 1), "Each set must non empty value option collection");
        }

        [TestMethod]
        public void UpsertDocumentAttributeValue()
        {
            var repo = new DocumentAttributeValueRepository(new UnitOfWork(Services.CONN_STRING));
            var entry = new DocumentAttributeValue() { DocumentId = 1, AttributeId = 1, ValueId = 1 };
            var args = new DocumentAttributeValue[] { entry };
            var result = repo.UpsertDocumentAttributeValues(args).Result;
            Assert.IsTrue(result, "Upsert must proceed");

            entry.AttributeId = 8;
            result = repo.UpsertDocumentAttributeValues(args).Result;
            Assert.IsFalse(result, "Upsert must not proceed");
        }
    }
}
